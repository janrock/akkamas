package org.yottalabs.functions

object ApiActorS3Fnc {

  def hello(): String = {
    "Hello S3 FNC! (" + new Status(Practice).getv1 + "," + new Status(Practice).getv2 + ")"
  }

}