package org.yottalabs.functions

object ApiActorS3SubFnc {

}

sealed trait Environment {

  def v1(): String
  def v2(): String

}

case object Practice extends Environment {

  val v1: String = "value1"
  val v2: String = "value2"

}

class Status(env: Environment) {

  val getv1: String = env.v1()
  val getv2: String = env.v2()

}