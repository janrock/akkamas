package org.yottalabs.actors

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

class ApiActorS2 extends Actor {

  val system = ActorSystem("Sub_System")
  val ApiActorS3: ActorRef = system.actorOf(Props[ApiActorS3], name = "ApiActorS3")

  object Resp {

    case object Greet

    case object Done

  }

  override def preStart(): Unit = {
    val resp = system.actorOf(Props(new Resp), "resp")
    resp ! Resp.Greet
  }

  class Resp extends Actor {
    def receive: PartialFunction[Any, Unit] = {
      case Resp.Greet =>
        println("Ping!")
        sender ! Resp.Done
    }
  }

  def receive: PartialFunction[Any, Unit] = {
    case "S2P1" =>
      println("Hello S2")
      ApiActorS3 ! "S3P1"
    case Resp.Done =>
      println("Pong!")

    case _ => println("Error(S2)!")
  }

}