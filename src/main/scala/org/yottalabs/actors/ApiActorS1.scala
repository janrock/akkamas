package org.yottalabs.actors

import akka.actor.Actor

class ApiActorS1 extends Actor {

  def receive: PartialFunction[Any, Unit] = {
    case "S1P1" =>
      println("Hello S1")
    case _ => println("Error(S1)!")
  }

}


//package org.yottalabs.actors
//
//import akka.actor.{Actor, Props}
//
//class ApiActor extends Actor {
//
//  override def preStart(): Unit = {
//    val greeter = context.actorOf(Props(new Greeter), "greeter")
//    greeter ! Greeter.Greet
//  }
//
//  def receive: PartialFunction[Any, Unit] = {
//    case "execute" => println("Hello!")
//    case Greeter.Done => context.stop(self)
//    case _ => println("Bye!")
//  }
//
//  object Greeter {
//
//    case object Greet
//    case object Done
//
//  }
//
//  class Greeter extends Actor {
//    def receive = {
//      case Greeter.Greet =>
//        println("Hello World!")
//        sender ! Greeter.Done
//    }
//  }
//
//}