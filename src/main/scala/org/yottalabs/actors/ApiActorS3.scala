package org.yottalabs.actors

import akka.actor.Actor
import org.yottalabs.functions.ApiActorS3Fnc._

class ApiActorS3 extends Actor {

  def receive: PartialFunction[Any, Unit] = {
    case "S3P1" =>
      println("Hello S3")
      println(hello())

    case _ => println("Error(S3)!")
  }

}