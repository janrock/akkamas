package org.yottalabs

import akka.actor.{ActorRef, ActorSystem, Props}
import org.yottalabs.actors.{ApiActorS1, ApiActorS2}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Main {

  val system = ActorSystem("System")
  val ApiActorS1: ActorRef = system.actorOf(Props[ApiActorS1], name = "ApiActorS1")
  val ApiActorS2: ActorRef = system.actorOf(Props[ApiActorS2], name = "ApiActorS2")

  def main(args: Array[String]): Unit = {
    system.scheduler.scheduleWithFixedDelay(0 seconds, 1 seconds, ApiActorS1, "S1P1")
    system.scheduler.scheduleWithFixedDelay(0 seconds, 2 seconds, ApiActorS2, "S2P1")
  }

}
