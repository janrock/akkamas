name := "akkamas"

version := "0.1"

scalaVersion := "2.12.8"

// http
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.7"
libraryDependencies += "org.apache.httpcomponents" % "httpcore" % "4.4.11"

// akka
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.23"
libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.8"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.6.0-M3"

// utils
libraryDependencies += "joda-time" % "joda-time" % "2.10.2"
libraryDependencies += "org.json4s" % "json4s-native_2.12" % "3.5.1"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.6"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.3.0"

libraryDependencies ++= Seq(
  "com.h2database"  %  "h2" % "1.3.+",
  "com.typesafe.slick" %% "slick" % "3.3.2",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.scalatest" %% "scalatest" % "3.0.8" % Test
)

libraryDependencies += "org.postgresql" % "postgresql" % "42.2.6"

libraryDependencies ++= {
  val circeV = "0.8.0"
  Seq(
    "io.circe" %% "circe-core" % circeV,
    "io.circe" %% "circe-generic" % circeV,
    "io.circe" %% "circe-parser" % circeV,
    "io.circe" %% "circe-java8" % circeV,
    "io.circe" %% "circe-generic-extras" % circeV,
    "io.circe" %% "circe-yaml" % circeV
  )
}

mainClass in assembly := some("org.yottalabs.Main")
assemblyJarName := "akkamas.jar"

// Merging strategy
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

fork in run := true
javaOptions in run ++= Seq(
  "-Dlog4j.debug=true",
  "-Dlog4j.configuration=log4j.properties")
outputStrategy := Some(StdoutOutput)

enablePlugins(DockerPlugin)

imageNames in docker := Seq(ImageName("yottalabs/akkamas-k8s:1.0"))

dockerfile in docker := {
  val jarFile: File = sbt.Keys.`package`.in(Compile, packageBin).value
  val classpath = (managedClasspath in Compile).value
  val mainclass = mainClass.in(Compile, packageBin).value.getOrElse(sys.error("Expected exactly one main class"))
  val jarTarget = s"/app/${jarFile.getName}"
  // Make a colon separated classpath with the JAR file
  val classpathString = classpath.files.map("/app/" + _.getName)
    .mkString(":") + ":" + jarTarget
  new Dockerfile {
    // Base image
    from("openjdk:8-jre")
    // Add all files on the classpath
    add(classpath.files, "/app/")
    // Add the JAR file
    add(jarFile, jarTarget)
    // On launch run Java with the classpath and the main class
    entryPoint("java", "-cp", classpathString, mainclass)
  }
}